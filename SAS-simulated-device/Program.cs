﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Devices.Client;
using Newtonsoft.Json;


namespace SAS
{
    class Program
    {
        /* 
         **  IoT Hub settings 
         */
        // IoT Hub hostname
        static private string IotHubUri = "{iot hub hostname}";
        // Device secret key
        static private string DeviceKey = "{device key}";
        // Device id
        static private string DeviceId = "myFirstDevice";


        private const double MinTemperature = 20;
        private const double MinHumidity = 60;
        private static readonly Random Rand = new Random();
        private static DeviceClient _deviceClient;
        private static int _messageId = 1;

        private static async void SendDeviceToCloudMessagesAsync()
        {
            

            while (true)
            {
                var currentTemperature = MinTemperature + Rand.NextDouble() * 15;
                var currentHumidity = MinHumidity + Rand.NextDouble() * 20;

                var telemetryDataPoint = new
                {
                    messageId = _messageId++,
                    deviceId = DeviceId,
                    temperature = currentTemperature,
                    humidity = currentHumidity
                };
                var messageString = JsonConvert.SerializeObject(telemetryDataPoint);
                var message = new Message(Encoding.ASCII.GetBytes(messageString));
                message.Properties.Add("temperatureAlert", (currentTemperature > 30) ? "true" : "false");

                await _deviceClient.SendEventAsync(message);
                Console.WriteLine("{0} > Sending message: {1}", DateTime.Now, messageString);

                await Task.Delay(1000);
            }
        }

        private static void Main(string[] args)
        {
            Console.Write("IoT Hub hostname: ");
            IotHubUri = Console.ReadLine();
            Console.Write("Device secret key: ");
            DeviceKey = Console.ReadLine();
            Console.Write("Device id: ");
            DeviceId = Console.ReadLine();


            Console.WriteLine("Simulated device\n");

            /* Authentication using SAS token ( = DeviceKey ) */
            _deviceClient = DeviceClient.Create(IotHubUri, new DeviceAuthenticationWithRegistrySymmetricKey(DeviceId, DeviceKey), TransportType.Mqtt);
            _deviceClient.ProductInfo = "HappyPath_Simulated-CSharp";

            SendDeviceToCloudMessagesAsync();
            Console.ReadLine();
        }
    }
}
