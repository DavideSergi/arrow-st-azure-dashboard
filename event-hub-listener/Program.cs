﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.EventHubs;
using Newtonsoft.Json;


namespace EventsListener
{

    class Program
    {
        private static string EventHubNamespaceConnectionString = null;
        private static string EventHubInstanceName = null;
        private static string ConsumerGroup = null;
        private static string PartitionId = null;

        private static void Main(string[] args)
        {
            /*
            ** In IoT Hub case, eventhub namespace connection string is 
            ** Endpoint={Event Hub-compatible endpoint};SharedAccessKeyName={iot hub policy name};SharedAccessKey={iot hub policy key}
            */
            Console.Write("EventHub namespace connection string: ");
            EventHubNamespaceConnectionString = Console.ReadLine();
            Console.Write("EventHub instance name: ");
            EventHubInstanceName = Console.ReadLine();
            Console.Write("Consumer group: ");
            ConsumerGroup = Console.ReadLine();
            Console.Write("Partition id: ");
            PartitionId = Console.ReadLine();

            IEnumerable<EventData> messages = null;
            // Create an event hub connector
            EventHubsConnectionStringBuilder connectionStringBuilder = new EventHubsConnectionStringBuilder(EventHubNamespaceConnectionString)
            {
                EntityPath = EventHubInstanceName
            };

            EventHubClient eventHubClient = EventHubClient.CreateFromConnectionString(connectionStringBuilder.ToString());
            PartitionReceiver receiver = eventHubClient.CreateReceiver(ConsumerGroup,PartitionId, EventPosition.FromEnd() );
            bool failed = false;
            while (true)
            {
                if(!failed)
                    Console.WriteLine("{0} > Waiting for messages...",DateTime.Now);
                // Start to receive message from event hub
                try
                {
                    Task<IEnumerable<EventData>> messagesTasks = receiver.ReceiveAsync(10);
                    messagesTasks.Wait();
                    messages = messagesTasks.Result;
                    List<EventData> list = new List<EventData>(messages);
                    failed = false;
                    // Process messages received
                    foreach (EventData message in list)
                    {
                        var messageString = Encoding.Default.GetString(message.Body.Array);
                        Console.WriteLine("{0} > Received message: {1}", DateTime.Now, messageString);  
                    }
                }
                catch (Exception e)
                {
                    failed=true;

                    // Error occurred 
                    Console.WriteLine("Reading from event hub failed.");
                    continue;
                }
            }
        }
    }
}