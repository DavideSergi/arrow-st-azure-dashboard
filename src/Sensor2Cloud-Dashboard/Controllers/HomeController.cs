using CTDWeb.Models;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Shared;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Shared.Enum;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CTDWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDeviceManager _deviceManager;
        private readonly IAzureIoTHubService _iotHubService;

        public HomeController(IDeviceManager deviceManager, IAzureIoTHubService iotHubService)
        {
            _deviceManager = deviceManager;
            _iotHubService = iotHubService;
        }

        public async Task<ActionResult> Index(string id)
        {
            TelemetryModel model = new TelemetryModel();

            if (id != null)
            {
                model.MAC = id.ToUpper();

                //Store the mac address to a session
                Session["MAC"] = id;
                model.MAC = (string)Session["MAC"];

                var twin = await _deviceManager.GetTwin(id);
                string status = string.Empty;

                if (twin == null)
                {
                    // probably device not registered
                    TempData["DeviceNotFound"] = "Device " + id + " not found";
                    return RedirectToAction("Insert", "Home");
                }

                // ARROW EW19 PATCH
                try
                {
                // \ ARROW EW19 PATCH
                    // Arrow patch
                    var patch = new
                    {
                        tags = new { boardType = twin.Properties.Reported["boardType"] },
                    };
                    twin = await _deviceManager.UpdateTwin(id, JsonConvert.SerializeObject(patch));
                    // \ Arrow patch

                // ARROW EW19 PATCH
                }
                catch (Exception ex) {
                    // no board type specified...
                }
                // \ ARROW EW19 PATCH

                try
                {
                    status = twin.Properties.Desired["status"];
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    // this device has not status property
                    twin = await SetDefaultStatus(id, twin.ETag);
                    status = twin.Properties.Desired["status"];
                }
                finally
                {
                    model.Status = status;

                    BoardType boardType;
                    try
                    {
                        boardType = twin.Tags["boardType"];
                        model.BoardType = EnumHelper.GetBoardTypeName(boardType);
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        // No board type found in twin
                        model.BoardType = string.Empty;
                    }

                }
            }
            else
            {
                return RedirectToAction("Insert", "Home");
            }

            return View(model);
        }

        public ActionResult Insert()
        {
            InsertModel model = new InsertModel()
            {
                MAC1 = "",
                MAC2 = "",
                MAC3 = "",
                MAC4 = "",
                MAC5 = "",
                MAC6 = ""
            };

            var mac = Session["MAC"] as string;

            if (!string.IsNullOrEmpty(mac) && mac.Length == 12)
            {
                model.MAC1 = mac.Substring(0, 2);
                model.MAC2 = mac.Substring(2, 2);
                model.MAC3 = mac.Substring(4, 2);
                model.MAC4 = mac.Substring(6, 2);
                model.MAC5 = mac.Substring(8, 2);
                model.MAC6 = mac.Substring(10, 2);
            }

            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public async Task<ActionResult> DeviceManagement(string id)
        {
            if (id != null)
            {
                Session["MAC"] = id;

                var twin = await _deviceManager.GetTwin(id);

                if (twin == null)
                {
                    TempData["DeviceNotFound"] = "Device " + id + " not found";
                    return RedirectToAction("Insert", "Home");
                }

                DeviceManagementModel model = new DeviceManagementModel();
                model.MAC = id;
                
                SetBoardType(ref model, ref twin);
                SetHasFWUpdate(ref model, ref twin);
                SetDirectMethods(ref model, ref twin);
                SetC2DMessageList(ref model, ref twin);

                return View(model);
            }
            else
            {
                return RedirectToAction("Insert", "Home");
            }

        }

        [HttpPost]
        public async Task<ActionResult> Upload(HttpPostedFileBase file)
        {
            string id = Request["id"];

            try
            {
                if (file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    //string path = Path.Combine(Server.MapPath("~/App_Data"), fileName);
                    //file.SaveAs(path);
                    
                    CloudBlobContainer container = GetContainer("firmware-uploaded");

                    //upload
                    CloudBlockBlob blob = container.GetBlockBlobReference(id + "/" + fileName);
                    await blob.UploadFromStreamAsync(file.InputStream);
                    
                    TempData["upload"] = "File uploaded successfully";


                }
            }
            catch(Exception e) {
                TempData["upload"] = "Error during file upload: " +e;

            }

            return RedirectToAction("/DeviceManagement/" + id);

        }

        [HttpPost]
        public async Task<JsonResult> InvokeDirectMethod(DirectMethodInput input)
        {
            CloudToDeviceMethodResult result = null;

            try
            {
                var requestPayload = JsonConvert.SerializeObject(new { });
                Debug.WriteLine(requestPayload);

                result = await _iotHubService.SendDirectMethod(input.Mac, input.DirectMethod, requestPayload, 10);

                Debug.WriteLine("Response status: {0}, payload:", result.Status);
                Debug.WriteLine(result.GetPayloadAsJson());
            }
            catch (Exception ex)
            {
                return Json(string.Format("Error invoking method {0}: {1}", input.DirectMethod, ex.Message), JsonRequestBehavior.AllowGet);
            }

            return Json(string.Format("Direct method {0} invoked successfully.", input.DirectMethod), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public async Task<string> GetTwin(string mac)
        {
            var twin = await _deviceManager.GetTwin(mac);
            var json = twin.ToJson();
            return json;
        }

        public async Task<JsonResult> TwinUpdate(TwinUpdateInput input)
        {
            try
            {
                // ARROW EW19 PATCH
                // input.Twin = await RestoreOriginalBoardType(input);
                // \ ARROW EW19 PATCH
                
                // Update twin
                var twin = await _deviceManager.UpdateTwin(input.Mac, input.Twin);

                var json = twin.ToJson();
                return Json(new { success = true, data = json }, JsonRequestBehavior.AllowGet);
            }
            catch (ArgumentException ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { success = false, error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { success = false, error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<JsonResult> FirmwareUpdate(FirmwareUpdateInput input)
        {
            CloudToDeviceMethodResult result = null;

            string fwUri = GetFirmwareUri(input);

            try
            {
                var requestPayload = JsonConvert.SerializeObject(new { FwPackageUri = fwUri });
                result = await _iotHubService.SendDirectMethod(input.Mac, "FirmwareUpdate", requestPayload);
                
                //if match input.Mac in fwUri vuol dire che � stato aggiunto dall'utente e posso rimuovere il firmware.
                //String searchString = input.Mac;
                //int startIndex = fwUri.IndexOf(searchString);
                //if (startIndex > -1)
                //{
                //    int endIndex = fwUri.IndexOf("?");//fwUri.Length; //deleting sas
                //    String substring = fwUri.Substring(startIndex, endIndex - startIndex);
                //    CloudBlobContainer container = GetContainer("firmware-uploaded");
                //    CloudBlockBlob blob = container.GetBlockBlobReference(substring);
                //    blob.Delete();
                //}
            }
            catch (Exception ex)
            {
                return Json("Error sending firmware update direct method. " + ex.Message, JsonRequestBehavior.AllowGet);
            }

            return Json("Firmware update sent successfully.", JsonRequestBehavior.AllowGet);
        }
        private void SetHasFWUpdate(ref DeviceManagementModel model, ref Twin twin)
        {
            model.HasFirmwareUpdate = false;
            try
            {
                var supportedMethods = (JsonConvert.DeserializeObject(twin.Properties.Reported["SupportedMethods"].ToString()) as JObject).Properties();
                foreach (var i in supportedMethods)
                {
                    if (Equals(i.Name, "FirmwareUpdate--FwPackageUri-string"))
                    {

                        // ARROW EW19 PATCH
                        //if (model.BoardType != string.Empty)
                            model.HasFirmwareUpdate = true;
                        //else
                        //    model.HasFirmwareUpdate = false;
                        // \ ARROW EW19 PATCH
                    }
                }

            }
            catch (ArgumentOutOfRangeException)
            {
                model.HasFirmwareUpdate = false;
            }
        }


        private void SetBoardType(ref DeviceManagementModel model, ref Twin twin)
        {
            // ARROW EW19 PATCH
            //BoardType boardType;
            //try
            //{
            //    boardType = twin.Tags["boardType"];
            //    model.BoardType = EnumHelper.GetBoardTypeName(boardType);

            //    List<string> firmwareList = GetFirmwareList(boardType, model.MAC);
            //    //if (firmwareList.Count > 0)
            //    //    model.HasFirmwareUpdate = true;

            //    model.FirmwareList = firmwareList;
            //}
            //catch (ArgumentOutOfRangeException)
            //{
            //    // No board type found in twin
            //    model.BoardType = string.Empty;
            //}

            List<string> firmwareList = GetFirmwareListWithoutBoardType(model.MAC);
            model.FirmwareList = firmwareList;
            // \ ARROW EW19 PATCH
        }
        private void SetC2DMessageList(ref DeviceManagementModel model, ref Twin twin)
        {
            try
            {
                var supportedCommands = (JsonConvert.DeserializeObject(twin.Properties.Reported["SupportedCommands"].ToString()) as JObject).Properties();

                model.C2DMessageList = supportedCommands
                    .Select(sm => sm.Name)
                    .ToList();


            }
            catch (ArgumentOutOfRangeException)
            {
                model.C2DMessageList = new List<string>();
            }
        }

        private void SetDirectMethods(ref DeviceManagementModel model, ref Twin twin)
        {
            try
            {
                var supportedMethods = (JsonConvert.DeserializeObject(twin.Properties.Reported["SupportedMethods"].ToString()) as JObject).Properties();

                model.DirectMethods = supportedMethods
                    .Where(sm => sm.Name.Contains("FirmwareUpdate") == false) // to avoid update
                    .Select(sm => sm.Name)
                    .ToList();


            }
            catch (ArgumentOutOfRangeException)
            {
                model.DirectMethods = new List<string>();
            }
        }
        private async Task<string> RestoreOriginalBoardType(TwinUpdateInput input)
        {
            // Get existing twin
            var twin = await _deviceManager.GetTwin(input.Mac);

            JObject patchObj = JObject.Parse(input.Twin);
            patchObj["tags"]["boardType"] = twin.Tags["boardType"];

            return patchObj.ToString();
        }

        private string GetFirmwareUri(FirmwareUpdateInput input)
        {

            CloudBlobContainer containerUploaded = GetContainer("firmware-uploaded");

            var firmwareUploaded = containerUploaded
                .ListBlobs(input.Mac + "/" + input.FirmwareId , true)
                .FirstOrDefault(b => b.Uri.Segments.Last().Contains(HttpUtility.UrlPathEncode(input.FirmwareId)));
            
            if(firmwareUploaded != null)
            {
                return firmwareUploaded.Uri.AbsoluteUri;
                //return GetBlobSasUri(containerUploaded, input.Mac + "/" + input.FirmwareId);
            }
            else
            {
                CloudBlobContainer container = GetContainer("firmware");

                string prefix = GetBoardName(input.BoardType);

                var firmware = container
                    .ListBlobs(prefix, true)
                    .FirstOrDefault(b => b.Uri.Segments.Last().Contains(input.FirmwareId));
              
                return firmware.Uri.AbsoluteUri;
            }
            
        }

        private static string GetBoardName(BoardType boardType)
        {

            string prefix = string.Empty;

            // select blob storage folder based on board type
            switch (boardType)
            {
                case BoardType.NucleoF401RE:
                    prefix = "NucleoF401RE";
                    break;
                case BoardType.NucleoL476RG:
                    prefix = "NucleoL476RG";
                    break;
                case BoardType.NucleoF429ZI:
                    prefix = "NucleoF429ZI";
                    break;
                case BoardType.BL475EIOT01Ax:
                    prefix = "BL475EIOT01Ax";
                    break;
                case BoardType.BlueMSmobilegateway:
                    prefix = "BlueMSmobilegateway";
                    break;
                default:
                    break;
            }
            return prefix;
        }

        private List<string> GetFirmwareList(BoardType boardType, string mac)
        {
            List<string> firmwareList = new List<string>();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = GetContainer("firmware");

            string prefix = GetBoardName(boardType);

            // Loop over items within the container and output the length and URI.
            foreach (IListBlobItem item in container.ListBlobs(prefix, true))
            {
                if (item.GetType() == typeof(CloudBlockBlob))
                {
                    CloudBlockBlob blob = (CloudBlockBlob)item;

                    Debug.WriteLine("GET FIRMWARE - Block blob of length {0}: {1}", blob.Properties.Length, blob.Uri);

                    firmwareList.Add(blob.Uri.Segments.Last());
                }
                else if (item.GetType() == typeof(CloudPageBlob))
                {
                    CloudPageBlob pageBlob = (CloudPageBlob)item;

                    Debug.WriteLine("GET FIRMWARE - Page blob of length {0}: {1}", pageBlob.Properties.Length, pageBlob.Uri);

                }
                else if (item.GetType() == typeof(CloudBlobDirectory))
                {
                    CloudBlobDirectory directory = (CloudBlobDirectory)item;

                    Debug.WriteLine("GET FIRMWARE - Directory: {0}", directory.Uri);
                }
            }

            CloudBlobContainer containerUploaded = GetContainer("firmware-uploaded");
            foreach (IListBlobItem item in containerUploaded.ListBlobs(mac, true))
            {
                if (item.GetType() == typeof(CloudBlockBlob))
                {
                    CloudBlockBlob blob = (CloudBlockBlob)item;
                    firmwareList.Add(Server.UrlDecode(blob.Uri.Segments.Last()) );
                }
            }

            return firmwareList;
        }

        // ARROW EW19 PATCH 
        private List<string> GetFirmwareListWithoutBoardType(string mac)
        {
            List<string> firmwareList = new List<string>();

            CloudBlobContainer containerUploaded = GetContainer("firmware-uploaded");
            foreach (IListBlobItem item in containerUploaded.ListBlobs(mac, true))
            {
                if (item.GetType() == typeof(CloudBlockBlob))
                {
                    CloudBlockBlob blob = (CloudBlockBlob)item;
                    firmwareList.Add(Server.UrlDecode(blob.Uri.Segments.Last()));
                }
            }

            return firmwareList;
        }
        // \ ARROW EW19 PATCH 


        private async Task<Twin> SetDefaultStatus(string macAddress, string etag)
        {
            Console.WriteLine("no status -> enabled");

            var path = new
            {
                properties = new
                {
                    desired = new
                    {
                        status = "enabled"
                    }
                }
            };

            var twin = await _deviceManager.UpdateTwin(macAddress, JsonConvert.SerializeObject(path));
            return twin;
        }

        private static CloudBlobContainer GetContainer(string containerName)
        {
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            CloudConfigurationManager.GetSetting("StorageConnectionString"));

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);

            //just in case, check to see if the container exists, and create it if it doesn't
            //container.CreateIfNotExists();

            return container;
        }

        private static string GetBlobSasUri(CloudBlobContainer container, string blobName, string policyName = null)
        {
            string sasBlobToken;

            // Get a reference to a blob within the container.
            // Note that the blob may not exist yet, but a SAS can still be created for it.
            CloudBlockBlob blob = container.GetBlockBlobReference(blobName);

            if (policyName == null)
            {
                // Create a new access policy and define its constraints.
                // Note that the SharedAccessBlobPolicy class is used both to define the parameters of an ad-hoc SAS, and
                // to construct a shared access policy that is saved to the container's shared access policies.
                SharedAccessBlobPolicy adHocSAS = new SharedAccessBlobPolicy()
                {
                    // When the start time for the SAS is omitted, the start time is assumed to be the time when the storage service receives the request.
                    // Omitting the start time for a SAS that is effective immediately helps to avoid clock skew.
                    SharedAccessExpiryTime = DateTime.UtcNow.AddHours(24),
                    Permissions = SharedAccessBlobPermissions.Read //| SharedAccessBlobPermissions.Write
                };

                // Generate the shared access signature on the blob, setting the constraints directly on the signature.
                sasBlobToken = blob.GetSharedAccessSignature(adHocSAS);

                Console.WriteLine("SAS for blob (ad hoc): {0}", sasBlobToken);
                Console.WriteLine();
            }
            else
            {
                // Generate the shared access signature on the blob. In this case, all of the constraints for the
                // shared access signature are specified on the container's stored access policy.
                sasBlobToken = blob.GetSharedAccessSignature(null, policyName);

                Console.WriteLine("SAS for blob (stored access policy): {0}", sasBlobToken);
                Console.WriteLine();
            }

            // Return the URI string for the container, including the SAS token.
            return blob.Uri + sasBlobToken;
        }

    }
}