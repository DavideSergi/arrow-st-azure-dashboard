﻿using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Http;
using System.Web.UI.WebControls;
using System.Diagnostics;
using Microsoft.WindowsAzure;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using CTDWeb.Models;
using System.Web.Configuration;
using System.IO;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage;

namespace CTDWeb
{
    public struct EventHubSettings
    {
        public string name { get; set; }
        public string connectionString { get; set; }
        public string consumerGroup { get; set; }
        public int partitionCount { get; set; }
    }

    public struct GlobalSettings
    {
        public bool ForceSocketCloseOnUserActionsTimeout { get; set; }
    }

    public class MvcApplication : System.Web.HttpApplication
    {
        EventHubSettings eventHubDevicesSettings;
        public static GlobalSettings globalSettings;

        protected void Application_Start()
        {
            RouteTable.Routes.MapHttpRoute(
               name: "DefaultApi",
               routeTemplate: "api/{controller}/{id}",
               defaults: new { id = System.Web.Http.RouteParameter.Optional }
           );

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Read connectiong strings and Event Hubs names from app.config file
            GetAppSettings();

            // Create EventProcessorHost clients
            CreateEventProcessorHostClient(ref eventHubDevicesSettings);

        }

        protected void Application_End(Object sender, EventArgs e)
        {
            Trace.TraceInformation("Unregistering EventProcessorHosts");
        }

        private void GetAppSettings()
        {
            try
            {
                globalSettings.ForceSocketCloseOnUserActionsTimeout =
                    CloudConfigurationManager.GetSetting("ForceSocketCloseOnUserActionsTimeout") == "true";
            }
            catch (Exception)
            {
            }

            // Read settings for Devices Event Hub
            eventHubDevicesSettings.connectionString = WebConfigurationManager.ConnectionStrings["Microsoft.ServiceBus.ConnectionStringDevices"].ConnectionString;
            eventHubDevicesSettings.name = CloudConfigurationManager.GetSetting("Microsoft.ServiceBus.EventHubDevices").ToLowerInvariant();
            eventHubDevicesSettings.partitionCount = int.Parse(CloudConfigurationManager.GetSetting("Microsoft.ServiceBus.EventHubDevices.PartitionCount"));
            eventHubDevicesSettings.consumerGroup = CloudConfigurationManager.GetSetting("Microsoft.ServiceBus.EventHubDevices.ConsumerGroup");
        }


        private void CreateEventProcessorHostClient(ref EventHubSettings eventHubSettings)
        {
            Trace.TraceInformation("Creating EventProcessorHost: {0}, {1}, {2}", this.Server.MachineName, eventHubSettings.name, eventHubSettings.consumerGroup);

            for (int i = 0; i < eventHubSettings.partitionCount; i++)
            {
                BackgroundThread.StartEHReceiver(eventHubSettings, i);
            }
        }

        /// <summary>
        /// Handle all application exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        async void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

            // Get the exception object.
            Exception exc = Server.GetLastError();

            await WriteExceptionToBlobStorage(exc);
        }

        private async static Task WriteExceptionToBlobStorage(Exception ex)
        {
            var container = GetContainer("exceptions-dashboard");
            container.CreateIfNotExists();

            var blob = container.GetBlockBlobReference(string.Format("exception-{0}.log", DateTime.UtcNow.Ticks));

            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(ex.ToString());
            writer.Flush();
            stream.Position = 0;

            await blob.UploadFromStreamAsync(stream);

            Console.WriteLine("Exception occurred and logged into azure blob storage");
        }

        private static CloudBlobContainer GetContainer(string containerName)
        {
            string storageConnectionString = WebConfigurationManager.ConnectionStrings["StorageConnectionString"].ConnectionString;

            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount =
                CloudStorageAccount.Parse(storageConnectionString);

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);

            //just in case, check to see if the container exists, and create it if it doesn't
            //container.CreateIfNotExists();

            return container;
        }
    }
}
