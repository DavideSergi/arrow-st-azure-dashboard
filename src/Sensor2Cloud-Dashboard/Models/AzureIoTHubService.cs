﻿using Microsoft.Azure.Devices;
using Microsoft.WindowsAzure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTDWeb.Models
{
    public class AzureIoTHubService : IAzureIoTHubService
    {
        private ServiceClient _serviceClient;
        private String _iothubConnectionString;

        public AzureIoTHubService(string iothubConnectionString)
        {
            _iothubConnectionString = iothubConnectionString;
        }
        

        public async Task SendCloudToDeviceMessageAsync(string deviceId, string message)
        {
            using (ServiceClient _serviceClient = ServiceClient.CreateFromConnectionString(_iothubConnectionString))
            {
                var commandMessage = new Message(Encoding.ASCII.GetBytes(message));
                await _serviceClient.SendAsync(deviceId, commandMessage);
            }     
        }

        public async Task<CloudToDeviceMethodResult> SendDirectMethod(string deviceId, string methodName, string requestPayload, double timeout = 30)
        {

            using (ServiceClient _serviceClient = ServiceClient.CreateFromConnectionString(_iothubConnectionString))
            {
                try
                {
                    var method = new CloudToDeviceMethod(methodName) { ResponseTimeout = TimeSpan.FromSeconds(timeout) };
                    method.SetPayloadJson(requestPayload);
                    return await _serviceClient.InvokeDeviceMethodAsync(deviceId, method);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
        }
    }
}