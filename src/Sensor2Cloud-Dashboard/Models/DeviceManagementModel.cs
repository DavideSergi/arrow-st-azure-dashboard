﻿using Shared.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTDWeb.Models
{
    public class DeviceManagementModel : DeviceModel
    {
        public bool HasFirmwareUpdate { get; set; } = false;
        public List<string> FirmwareList { get; set; }
        //public C2DMessage C2DMessages { get; set; }
        public IList<string> DirectMethods { get; set; }
        public IList<string> C2DMessageList { get; set; }
    }
}