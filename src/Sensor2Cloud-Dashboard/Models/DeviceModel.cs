﻿using Microsoft.WindowsAzure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTDWeb.Models
{
    public class DeviceModel
    {
        public DeviceModel()
        {
            API_URL = CloudConfigurationManager.GetSetting("API.URL");
            BING_SPEECH_API_KEY = CloudConfigurationManager.GetSetting("BING_SPEECH_API_KEY");
            LUIS_APP_ID = CloudConfigurationManager.GetSetting("LUIS_APP_ID");
            LUIS_SUBSCRIPTION_ID = CloudConfigurationManager.GetSetting("LUIS_SUBSCRIPTION_ID");
        }

        public string MAC { get; set; }
        public string BoardType { get; set; }
        public string API_URL { get; set; } = CloudConfigurationManager.GetSetting("API.URL");
        public string BING_SPEECH_API_KEY { get; set; } = CloudConfigurationManager.GetSetting("BING_SPEECH_API_KEY");
        public string LUIS_APP_ID { get; set; } = CloudConfigurationManager.GetSetting("LUIS_APP_ID");
        public string LUIS_SUBSCRIPTION_ID { get; set; } = CloudConfigurationManager.GetSetting("LUIS_SUBSCRIPTION_ID");
    }
}