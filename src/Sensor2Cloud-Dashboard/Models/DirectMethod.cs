﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTDWeb.Models
{
    public class DirectMethod
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}