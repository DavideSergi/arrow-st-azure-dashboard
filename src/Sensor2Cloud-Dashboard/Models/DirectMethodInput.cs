﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTDWeb.Models
{
    public class DirectMethodInput
    {
        public string Mac { get; set; }

        public string DirectMethod { get; set; }
    }
}