﻿using Shared.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTDWeb.Models
{
    public class FirmwareUpdateInput
    {
        public string Mac { get; set; }

        public string FirmwareId { get; set; }

        public BoardType BoardType { get; set; }
    }
}