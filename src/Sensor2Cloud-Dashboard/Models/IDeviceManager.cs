﻿using Microsoft.Azure.Devices;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Azure.Devices.Shared;

namespace CTDWeb.Models
{
    public interface IDeviceManager
    {
        Task enable(string mac);
        Task disable(string mac);
        Task remove(string mac);
        Task enableAll();
        Task disableAll();

        #region Twin

        IQuery CreateQuery(string query, int size = -1);
        Task<Twin> GetTwin(string mac);
        Task<Twin> UpdateTwin(string mac, string jsonTwinPatch);
        Task<Device> AddDevice(string deviceId);
        Task<Device> GetDevice(string deviceId);
        Task<IEnumerable<Device>> GetDevices(int maxCount);
        Task RemoveDevice(string deviceId);

        #endregion
    }
}
