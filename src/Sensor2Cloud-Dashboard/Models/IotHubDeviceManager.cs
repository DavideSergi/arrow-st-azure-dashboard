﻿using Microsoft.Azure.Devices;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Azure.Devices.Common.Exceptions;
using Microsoft.Azure.Devices.Shared;

namespace CTDWeb.Models
{
    public class IotHubDeviceManager : IDeviceManager
    {
        private RegistryManager registryManager;

        public IotHubDeviceManager(string connectionString)
        {
            registryManager = RegistryManager.CreateFromConnectionString(connectionString);
        }

        public async Task disable(string mac)
        {
            var device = await registryManager.GetDeviceAsync(mac);
            device.Status = DeviceStatus.Disabled;
            await registryManager.UpdateDeviceAsync(device);
        }

        public async Task enable(string mac)
        {
            var device = await registryManager.GetDeviceAsync(mac);
            device.Status = DeviceStatus.Enabled;
            await registryManager.UpdateDeviceAsync(device);
        }

        public async Task remove(string mac)
        {
            await registryManager.RemoveDeviceAsync(mac);
        }

        public async Task enableAll()
        {
            var stats = await registryManager.GetRegistryStatisticsAsync();
            Console.WriteLine("device count: " + stats.TotalDeviceCount + " enabled: " + stats.EnabledDeviceCount + " disabled: " + stats.DisabledDeviceCount);
            var devices = await registryManager.GetDevicesAsync((int)stats.TotalDeviceCount * 100); // * 100 because the device list is an approximation 
            devices = devices.Select(d => { d.Status = DeviceStatus.Enabled; return d; });
            await registryManager.UpdateDevices2Async(devices);
        }

        public async Task disableAll()
        {
            var devices = await registryManager.GetDevicesAsync(1);
            devices.Select(d => { d.Status = DeviceStatus.Disabled; return d; });
            await registryManager.UpdateDevices2Async(devices);
        }

        #region Twin

        public IQuery CreateQuery(string query, int size)
        {
            if (size != -1)
                return registryManager.CreateQuery(query, size);
            else
                return registryManager.CreateQuery(query);
        }

        public async Task<Twin> GetTwin(string deviceId)
        {
            var twin = await registryManager.GetTwinAsync(deviceId);
            return twin;
        }

        public async Task<Twin> UpdateTwin(string deviceId, string jsonTwinPatch)
        {
            var twin = await registryManager.GetTwinAsync(deviceId);
            twin = await registryManager.UpdateTwinAsync(deviceId, jsonTwinPatch, twin.ETag);
            return twin;
        }

        public async Task<Device> AddDevice(string deviceId)
        {
            var device = await registryManager.AddDeviceAsync(new Device(deviceId));
            return device;
        }

        public async Task<Device> GetDevice(string deviceId)
        {
            var device = await registryManager.GetDeviceAsync(deviceId);
            return device;
        }

        public async Task<IEnumerable<Device>> GetDevices(int maxCount)
        {
            var devices = await registryManager.GetDevicesAsync(maxCount);
            return devices;
        }

        public async Task RemoveDevice(string deviceId)
        {
            await registryManager.RemoveDeviceAsync(deviceId);
        }

        #endregion
    }
}
