﻿using Shared.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace CTDWeb.Models
{
    public class TelemetryModel : DeviceModel
    {
        public string Status { get; set; }
    }
}