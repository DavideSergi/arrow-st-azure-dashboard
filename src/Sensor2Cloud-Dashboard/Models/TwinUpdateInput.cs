﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTDWeb.Models
{
    public class TwinUpdateInput
    {
        public string Mac { get; set; }

        public string Twin { get; set; }
    }
}