﻿
/******************
* SPEECH TO TEXT *
******************/
var client;
var request;
var registerTimeMs = 3000;

function timer(ms, callback) {
    var elem = document.getElementById("timer-show");
    var initialWidth = 100;
    var width = initialWidth;
    var intervalDelay = 10;
    var intervalId = setInterval(frame, intervalDelay);

    var widthStepsWidth = initialWidth / (ms / intervalDelay);

    function frame() {
        if (width <= 0) {
            clearInterval(intervalId);
            elem.style.width = "";
            callback();
        } else {
            width -= widthStepsWidth;
            elem.style.width = width + '%';
        }
    }
}

function getMode() {
    return Microsoft.CognitiveServices.SpeechRecognition.SpeechRecognitionMode.shortPhrase;
}

function getLanguage() {
    return "it-it";
}

function setTranscriptText(text) {
    $("#speech_api_output").text(text);
}

function setIntentText(text) {
    $("#c2dMessage").val(text);
}

function clearText() {
    setTranscriptText('');
    setIntentText('');
}

function startSpeechToText(luisConfig, key) {
    var mode = getMode();
    var luisCfg = luisConfig;

    clearText();

    if (luisCfg) {
        client = Microsoft.CognitiveServices.SpeechRecognition.SpeechRecognitionServiceFactory.createMicrophoneClientWithIntent(
            getLanguage(),
            key,
            luisCfg.appid,
            luisCfg.subid);
    } else {
        client = Microsoft.CognitiveServices.SpeechRecognition.SpeechRecognitionServiceFactory.createMicrophoneClient(
            mode,
            getLanguage(),
            key);
    }
    client.startMicAndRecognition();
    $('#mic').addClass('listening');

    timer(registerTimeMs,
        function () {
            client.endMicAndRecognition();
            $('#mic').removeClass('listening');
        });

    client.onPartialResponseReceived = function (response) {
        setTranscriptText(response);
    }

    client.onFinalResponseReceived = function (response) {
        console.log("SPEECH_TO_TEXT FINAL RESPONSE", response);
        if (response && response[0] && response[0].transcript)
            setTranscriptText(response[0].transcript);
    }

    client.onIntentReceived = function (response) {
        var responseJSON = JSON.parse(response);
        console.log("SPEECH_TO_TEXT INTENT RECEIVED", responseJSON);
        if (responseJSON && responseJSON.intents && responseJSON.intents[0] && responseJSON.intents[0].score > 0.5)
            setIntentText(responseJSON.intents[0].intent);
    };
}
