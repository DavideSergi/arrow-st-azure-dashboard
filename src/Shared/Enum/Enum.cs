﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Enum
{
    public enum BoardType { NucleoF401RE = 1, NucleoL476RG, NucleoF429ZI, BL475EIOT01Ax, BlueMSmobilegateway }

    //public enum C2DMessage { LedOn, LedOff, LedBlink, Pause, Play }

    public static class EnumHelper
    {
        private static readonly IDictionary<BoardType, string> BoardTypeName = new Dictionary<BoardType, string>
        {
            { BoardType.NucleoF401RE, "Nucleo-F401RE" },
            { BoardType.NucleoL476RG, "Nucleo-L476RG " },
            { BoardType.NucleoF429ZI, "Nucleo-F429ZI" },
            { BoardType.BL475EIOT01Ax, "B-L475E-IOT01Ax " },
            { BoardType.BlueMSmobilegateway, "BlueMS mobile gateway" }
        };


        public static string GetBoardTypeName(BoardType boardType)
        {
            return BoardTypeName[boardType];
        }
    }
}
