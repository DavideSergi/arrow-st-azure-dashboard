﻿using System.ComponentModel.DataAnnotations;

namespace Shared.Helpers
{
    public class IsBoardTypeValidAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var val = (Enum.BoardType)value;

            bool result = System.Enum.IsDefined(typeof(Enum.BoardType), val);

            if (result == true)
                return ValidationResult.Success;

            return new ValidationResult("Board type is not valid");
        }
    }
}
