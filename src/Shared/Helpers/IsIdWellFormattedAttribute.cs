﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Helpers
{
    public class IsIdWellFormattedAttribute : ValidationAttribute
    {
        private IDeviceValidator DeviceValidator { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string id = value as string;

            if (string.IsNullOrEmpty(id))
                return new ValidationResult("ID must not be null or empty");

            string uuid = null;
            string mac = null;

            var r = id.Split('+');
            if (r.Length != 2)
                return new ValidationResult("ID format error");

            uuid = r[0];
            mac = r[1].ToUpper();

            if (!DeviceValidator.isValid(uuid))
                return new ValidationResult("Device \"" + uuid + "\" is not valid.");


            if (!DeviceValidator.isMACValid(mac))
                return new ValidationResult("Device with MAC \"" + uuid + "\" is not valid.");

            return ValidationResult.Success;
        }
    }
}
