﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shared.Helpers
{
    public class STDeviceValidator : IDeviceValidator
    {
        public bool isValid(string id)
        {
            return true;
        }

        public bool isMACValid(string mac)
        {
            return mac.Length == 12;
        }
    }
}